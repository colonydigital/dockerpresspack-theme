<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Blog" <?php language_attributes(); ?>>
	<head>

		<meta charset="<?php bloginfo('charset'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<title><?php wp_title('|',true,'right'); ?></title>

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link rel="icon" href="<?php bloginfo('template_directory'); ?>/favicon.png" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.png" type="image/x-icon" />

		<?php
			if(is_singular() && get_option('thread_comments')) wp_enqueue_script('comment-reply');
			wp_head();
		?>

		<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

	</head>

	<body <?php body_class(); ?>>

		<header id="header" class="max-width">

			<!-- Logo -->
			<div id="logo">
				<?php getLogo(function($logo){ ?>
					<a href="<?php echo home_url(); ?>" title="Go to Our Home Page: <?php bloginfo('name'); ?>">
						<img src="<?php bloginfo('template_directory'); ?>/logo-main.png" alt=" <?php bloginfo('name'); ?>" />
					</a>
				<?php }); ?>
			</div>


			<!-- Navigation -->
			<nav id="primary">
				<div class="menu-btn flex-row-wrapper flex-middle flex-center">
					<span class="bar"></span>
				</div>
				<?php
					wp_nav_menu(array(
						'container_class' 	=> '',
						'theme_location' 	=> 'primary',
						'menu_class' 		=> 'flex-row-wrapper',
						'walker' 			=> new Menu_With_Description
					));
	            ?>
			</nav>

		</header>
