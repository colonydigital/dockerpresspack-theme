		<!-- Footer -->	
		<footer id="footer">

			<!-- Colony Signature -->
			<a href="https://colonydigital.ca" title="Colony Digital" data-author="Colony Digital" target="_blank">
				<span>Site By</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.1 62.8" style="height:18px;">
					<title>Colony</title>
					<g>
						<g>
							<path d="M25.1,0V23.1H16.7V8.4H8.3v46h8.4V39.7h8.4V62.8H0V0Z" />
						</g>
					</g>
				</svg>
			</a>

		</footer>

		<?php wp_footer(); ?>
	
	</body>

</html>