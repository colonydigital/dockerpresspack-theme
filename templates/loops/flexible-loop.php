<?php
if(have_rows('flexible_layouts')):

    while(have_rows('flexible_layouts')): the_row();

        get_template_part( 'templates/flexible-layouts/' . get_row_layout() );

    endwhile;

else:


endif;
?>