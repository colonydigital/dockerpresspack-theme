<?php
/* Template Name: Flexible Page Layout */
/* Template Post Type: post, page */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<?php get_template_part('templates/loops/flexible-loop'); ?>

	</main> <!-- #main -->
</div> <!-- #primary -->


<?php get_footer(); ?>